import functools, multiprocessing, tqdm
from dsgrn_net_query import CountFPMatch as fpmatch
from dsgrn_design.component_logic_library import collect_logic, build_reg_dict
import DSGRN
from dsgrn_utilities import get_parameter_neighbors as neighbors


def get_neighbors(ess_netspec, parameter_matches):
    _,noness_net_spec = neighbors.make_nonessential(ess_netspec)
    ess_pg = DSGRN.ParameterGraph(DSGRN.Network(ess_netspec))
    # the following function returns neighboring indices that are NOT in the parameter_matches list
    new_match_indices, nbr_indices = neighbors.get_parameter_neighbors_from_list_in_nonessential_pg(ess_pg,parameter_matches)
    return noness_net_spec, nbr_indices


def check_circuit_behavior(included_bounds, excluded_bounds,network_spec):
    network = DSGRN.Network(network_spec)
    parameter_graph = DSGRN.ParameterGraph(network)
    param_dict = {}
    for param_index in range(parameter_graph.size()):
        parameter = parameter_graph.parameter(param_index)
        stable_FP_annotations = fpmatch.DSGRN_Computation(parameter)
        if fpmatch.all_included(network, included_bounds, stable_FP_annotations) and fpmatch.all_excluded(network, excluded_bounds, stable_FP_annotations):
            reg_dict = build_reg_dict(network)
            logics = collect_logic(network,parameter_graph.parameter(param_index),reg_dict) # dict of logic functions
            if logics is not None:
                param_dict[param_index] = logics
    return network_spec,param_dict


def check_neighbor_scores(included_bounds, excluded_bounds,net_params):
    # net_params is a tuple of a network specification and a list of parameter indices to find neighbors in
    # nonessential parameter graph
    network_spec, parameter_matches = net_params
    noness_netspec, paramlist = get_neighbors(network_spec,parameter_matches)
    network = DSGRN.Network(noness_netspec)
    parameter_graph = DSGRN.ParameterGraph(network)
    count = 0
    for param_index in paramlist:
        parameter = parameter_graph.parameter(param_index)
        stable_FP_annotations = fpmatch.DSGRN_Computation(parameter)
        if fpmatch.all_included(network, included_bounds, stable_FP_annotations) and fpmatch.all_excluded(network, excluded_bounds, stable_FP_annotations):
            count += 1
    return network_spec,[count,len(paramlist)]


def do_circuit_search(network_specs, included_bounds, excluded_bounds,parameter_matches=None):
    # network_specs is a list, parameter_matches is either None or a list of lists of parameter indices the same length
    # as network_specs
    search_results = []
    pool = multiprocessing.Pool()
    if parameter_matches is None:
        func = functools.partial(check_circuit_behavior,included_bounds,excluded_bounds)
        for x in tqdm.tqdm(pool.imap_unordered(func, network_specs), total=len(network_specs)):
            search_results.append(x)
    else:
        if len(network_specs) != len(parameter_matches):
            raise ValueError("Every network must have a list of parameter matches.")
        inputs = zip(network_specs, parameter_matches)
        func = functools.partial(check_neighbor_scores, included_bounds, excluded_bounds)
        for x in tqdm.tqdm(pool.imap_unordered(func, inputs), total=len(network_specs)):
            search_results.append(x)
    results = dict(search_results)
    return results

