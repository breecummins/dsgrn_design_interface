import sbol2 as sbol
import ast
from functools import reduce
import datetime
from dsgrn_design import component_logic_library as cll

sbol.setHomespace("http://dsgrn_design.org")
sbol.Config.setOption('sbol_typed_uris', False)

# SBOL interaction types
inhibition = sbol.SBO_INHIBITION
stimulation = sbol.SBO_STIMULATION
control = sbol.SBO_CONTROL

# SBOL participation types
inhibitor = sbol.SBO_INHIBITOR
inhibited = sbol.SBO + "0000642"
stimulator = sbol.SBO_STIMULATOR
stimulated = sbol.SBO + "0000643"
modified = sbol.SBO + "0000644"


def read_dsgrn_net(network_file):
    netspec = open(network_file).read()
    rows = netspec.split("\n")
    while not rows[-1]:
        rows = rows[:-1]
    node_dict = {}
    for row in rows:
        rowterms = "".join(row.split()).split(":")
        node_dict[rowterms[0]] = rowterms[1]
    return node_dict


def construct_node_topology(topology):
    repls = {"(": " ", ")": " ","+":" "}
    topology = reduce(lambda a, kv: a.replace(*kv), repls.items(), topology)
    splits = topology.split()
    if splits:
        inputs, regs = zip(*[(t,"s") if t[0] != "~" else (t[1:],"i") for t in splits ])
    else:
        inputs, regs = [],[]
    return list(inputs), list(regs)


def make_comp_def(doc,mod,node_dict):
    for n in node_dict:
        comp = sbol.ComponentDefinition(n,sbol.SBO+"0000285")
        comp.name = n
        doc.addComponentDefinition(comp)
        fc = mod.functionalComponents.create("fc_{}".format(n))
        fc.definition = comp
        fc.name = n


def make_interactions(node_dict,mod,logic):
    ids = sorted(list(node_dict.keys()))
    input_topologies = {id : construct_node_topology(node_dict[id]) for id in ids}
    inputs = {id : input_topologies[id][0] for id in ids}
    regs = {id : input_topologies[id][1] for id in ids}
    for id in ids:
        if regs[id]:
            node_int = mod.interactions.create("int_{}".format(id))
            part = node_int.participations.create("part_{}".format(id))
            part.participant = mod.functionalComponents["fc_{}".format(id)]
            if set(regs[id]) == set(["s","i"]):
                node_int.types = [control,logic[id]]
                part.roles = [modified]
            elif set(regs[id]) == set("s"):
                node_int.types = [stimulation,logic[id]]
                part.roles = [stimulated]
            elif set(regs[id]) == set("i"):
                node_int.types = [inhibition,logic[id]]
                part.roles = [inhibited]
            else:
                raise ValueError("Regulation type unknown.")
            for input,reg in zip(inputs[id],regs[id]):
                make_participation(id, node_int, input, reg,mod)


def make_participation(id, node_int,input,reg,mod):
    part = node_int.participations.create("part_{}_{}".format(id,input))
    if reg == "s":
        part.roles = [stimulator]
    elif reg == "i":
        part.roles = [inhibitor]
    else:
        raise ValueError("Regulation type unknown.")
    if input:
        part.participant = mod.functionalComponents["fc_{}".format(input)]


def make_doc(network_file, logic, displayID = "DSGRN_Design", output_fname = "dsgrn_design.xml"):
    '''

    :param network_file: DSGRN network specification
    :param logic: dictionary of network nodes keying logical functions for Cello input
    example: { 'inducer' : 'NONE', 'Z' : 'ID', 'A' : 'NOR', 'B' : 'NOR', 'C' : 'NOR', 'reporter' : 'NOT' } (see e.g. networks/bistable_network_1.txt)
    For 0 input nodes, the logic is 'None.' For 1 input, it's ID or NOT. For two-input nodes the allowable logics are OR, AND, NOR, NAND, MIMPLY, CIMPLY, MNIMPLY, CNIMPLY, where MIMPLY means material implication and CNIMPLY means converse nonimplication. Three (and above) node inputs are not yet defined.
    :param displayID: descriptive beginning of string for doc.displayID
    :param output_fname: file name to save under
    :return: None, writes document to file
    '''
    doc = sbol.Document()
    # Create time stamp for unique names
    timestamp = datetime.datetime.now().isoformat()
    sanitized_timestamp = timestamp.replace(':', '_').replace('-', '_').replace('.', '_')
    doc.displayId = displayID + '_%s' % sanitized_timestamp
    doc.name = displayID
    doc.description = displayID
    mod = sbol.ModuleDefinition("dsgrn_design_%s" % sanitized_timestamp)
    doc.addModuleDefinition(mod)
    node_dict = read_dsgrn_net(network_file)
    make_comp_def(doc,mod,node_dict)
    if isinstance(logic,str):
        logic = ast.literal_eval(logic)
    logics = {}
    for node,log in logic.items():
        uri = cll.return_open_math(log)
        logics[node] = uri
    #module annotation
    make_interactions(node_dict, mod,logics)
    if doc.validate().startswith("V"):
        doc.write(output_fname)
        print("SBOL document generated.\n")
    else:
        print("SBOL document failed to validate.\n")
        print(doc.validate())


if __name__ == "__main__":
    # make_doc(*sys.argv[1:])
    logicdict = { "input1": "ID", "input2": "ID", "x1" : "ID", "x2" : "ID", "x3" : "NOT", "x4" : "NOT", "x5" : "NOR", "output" : "NOT"}
    make_doc("../../tests/NAND_original.txt", logicdict, displayID="DSGRN_Design_NAND")
