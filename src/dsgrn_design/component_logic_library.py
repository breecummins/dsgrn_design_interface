# The MIT License (MIT)
#
# Copyright (c) 2019 Bree Cummins
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


def return_open_math(logic):
    if logic == "NONE":
        return ""
    elif logic == "ID":
        return "https://www.openmath.org/cd/logic1#equivalent"
    elif logic == "NOT":
        return "https://www.openmath.org/cd/logic1#not"
    elif logic == "OR":
        return "https://www.openmath.org/cd/logic1#or"
    elif logic == "NOR":
        return "https://www.openmath.org/cd/logic1#nor"
    elif logic == "AND":
        return "https://www.openmath.org/cd/logic1#and"
    elif logic == "NAND":
        return "https://www.openmath.org/cd/logic1#nand"
    elif logic == "IMPLY" or "activator wins":
        return "https://www.openmath.org/cd/logic1#implies"
    elif logic == "NIMPLY" or "repressor wins":
        return "https://www.openmath.org/cd/logic1#not https://www.openmath.org/cd/logic1#implies"
    else:
        raise ValueError("Logic {} not recognized.".format(logic))


def logic2constitutive(logic):
    if logic in ["ID", "AND", "OR", "NIMPLY", "repressor wins"]:
        return "no"
    elif logic in ["NOT", "NAND", "NOR", "IMPLY", "activator wins"]:
        return "yes"
    else:
        return None


def hex2logic(reg_type,hexcode,num_out):
    #FIXME: add zero in
    if num_out not in [0,1,2,3]:
        raise ValueError("{} out-edges not implemented".format(num_out))
    if reg_type == "a":
        if (num_out <= 1 and hexcode == "2") or (num_out == 2 and hexcode == "C") or (num_out == 3 and hexcode == "38"):
            return "ID"
        else:
            return "ambiguous"
    elif reg_type == "r":
        if (num_out <= 1 and hexcode == "2") or (num_out == 2 and hexcode == "C") or (num_out == 3 and hexcode == "38"):
            return "NOT"
        else:
            return "ambiguous"
    elif reg_type == "aa":
        if (num_out <= 1 and hexcode=="E") or (num_out==2 and hexcode=="FC") or (num_out==3 and hexcode=="FF8"):
            return "OR"
        elif (num_out<=1 and hexcode=="8") or (num_out==2 and hexcode=="C0") or (num_out==3 and hexcode=="E00"):
            return "AND"
        else:
            return "ambiguous"
    elif reg_type == "rr":
        if (num_out <= 1 and hexcode == "E") or (num_out == 2 and hexcode == "FC") or (
                num_out == 3 and hexcode == "FF8"):
            return "NAND"
        elif (num_out <= 1 and hexcode == "8") or (num_out == 2 and hexcode == "C0") or (
                num_out == 3 and hexcode == "E00"):
            return "NOR"
        else:
            return "ambiguous"
    elif reg_type == "ar":
        if (num_out <= 1 and hexcode == "E") or (num_out == 2 and hexcode == "FC") or (num_out == 3 and hexcode == "FF8"):
            return "activator wins"
        elif (num_out <= 1 and hexcode == "8") or (num_out == 2 and hexcode == "C0") or (num_out == 3 and hexcode == "E00"):
            return "repressor wins"
        else:
            return "ambiguous"
    else:
        raise ValueError("Regulation type {} unrecognized.".format(reg_type))


def get_reg_type(network,node_index):
    # network is a DSGRN.Network object
    # node_index is an integer denoting a node in the network
    inputs = network.inputs(node_index)
    num_in = len(inputs)
    #FIXME: add zero in
    if num_in == 1:
        reg = network.interaction(inputs[0], node_index)
        if reg:
            return "a"
        else:
            return "r"
    if num_in == 2:
        reg = [network.interaction(j, node_index) for j in inputs]
        if len(set(reg)) == 1 and False in reg:
            return "rr"
        elif len(set(reg)) == 1 and True in reg:
            return "aa"
        elif len(set(reg)) == 2:
            return "ar"
        else:
            raise RuntimeError("All cases should have been exhausted.")
    else:
        return None


def build_reg_dict(network):
    interaction_dict = {}
    for node_index in range(network.size()):
        reg = get_reg_type(network,node_index)
        if reg is not None:
            interaction_dict[node_index] = reg
    return interaction_dict


def get_hexcode(parameter,node_index):
    # parameter is DSGRN.Parameter object
    # node_index is an integer denoting a node in the network
    return parameter.logic()[node_index].hex()


def get_num_out_edges(network,node_index):
    # network is a DSGRN.Network object
    # node_index is an integer denoting a node in the network
    return len(network.outputs(node_index))


def collect_logic(network,parameter,reg_dict):
    # network is a DSGRN.Network object
    # parameter is a DSGRN.Parameter object
    # reg_dict is a dictionary of node indices keying regulation type, activating or repressing
    # return dict of node name : logic
    logics = {}
    for node_index,reg in reg_dict.items():
        hc = get_hexcode(parameter,node_index)
        oe = get_num_out_edges(network,node_index)
        logics[network.name(node_index)] = (hex2logic(reg,hc,oe))
    return logics


def construct_FPs(logic):
    # overall logic for the circuit is a string
    #FIXME: These FPs only work if each input has exactly one downstream target. Will need to increase FP value from 2 to highest value by counting outedges.

    NOR = [{"input1" : [0,0], "input2" : [0,0], "output" : [1,1]},
           {"input1" : [0,0], "input2" : [2,2], "output" : [0,0]},
           {"input1" : [2,2], "input2" : [0,0], "output" : [0,0]},
           {"input1" : [2,2], "input2" : [2,2], "output" : [0,0]}]

    OR =  [{"input1" : [0,0], "input2" : [0,0], "output" : [0,0]},
           {"input1" : [0,0], "input2" : [2,2], "output" : [1,1]},
           {"input1" : [2,2], "input2" : [0,0], "output" : [1,1]},
           {"input1" : [2,2], "input2" : [2,2], "output" : [1,1]}]

    XNOR = [{"input1" : [0,0], "input2" : [0,0], "output" : [1,1]},
            {"input1" : [0,0], "input2" : [2,2], "output" : [0,0]},
            {"input1" : [2,2], "input2" : [0,0], "output" : [0,0]},
            {"input1" : [2,2], "input2" : [2,2], "output" : [1,1]}]

    XOR =  [{"input1" : [0,0], "input2" : [0,0], "output" : [0,0]},
            {"input1" : [0,0], "input2" : [2,2], "output" : [1,1]},
            {"input1" : [2,2], "input2" : [0,0], "output" : [1,1]},
            {"input1" : [2,2], "input2" : [2,2], "output" : [0,0]}]

    NAND = [{"input1" : [0,0], "input2" : [0,0], "output" : [1,1]},
            {"input1" : [0,0], "input2" : [2,2], "output" : [1,1]},
            {"input1" : [2,2], "input2" : [0,0], "output" : [1,1]},
            {"input1" : [2,2], "input2" : [2,2], "output" : [0,0]}]

    AND =  [{"input1" : [0,0], "input2" : [0,0], "output" : [0,0]},
            {"input1" : [0,0], "input2" : [2,2], "output" : [0,0]},
            {"input1" : [2,2], "input2" : [0,0], "output" : [0,0]},
            {"input1" : [2,2], "input2" : [2,2], "output" : [1,1]}]

    IMPLY = [{"input1" : [0,0], "input2" : [0,0], "output" : [1,1]},
             {"input1" : [0,0], "input2" : [2,2], "output" : [1,1]},
             {"input1" : [2,2], "input2" : [0,0], "output" : [0,0]},
             {"input1" : [2,2], "input2" : [2,2], "output" : [1,1]}]

    NIMPLY = [{"input1" : [0,0], "input2" : [0,0], "output" : [0,0]},
              {"input1" : [0,0], "input2" : [2,2], "output" : [0,0]},
              {"input1" : [2,2], "input2" : [0,0], "output" : [1,1]},
              {"input1" : [2,2], "input2" : [2,2], "output" : [0,0]}]

    if logic == "NOR": return NOR, OR
    elif logic == "OR": return OR, NOR
    elif logic == "XNOR": return XNOR, XOR
    elif logic == "XOR": return XOR, XNOR
    elif logic == "NAND": return NAND, AND
    elif logic == "AND": return AND, NAND
    elif logic == "IMPLY": return IMPLY, NIMPLY
    elif logic == "NIMPLY": return NIMPLY, IMPLY
    else: raise ValueError("Logic {} not implemented.".format(logic))


