import json, os, datetime
from dsgrn_design import component_logic_library as cll


def construct_seed(num_inducers,type_inducers,num_nodes):
    if num_nodes < num_inducers:
        raise ValueError("The number of inducers must be less than or equal to the number of interior gene products.\nFix interface parameters .json file.")
    inducer_vars = ["input{} : input{}".format(i,i) for i in range(1,num_inducers+1)]
    t = "~" if type_inducers.startswith("r") else ""
    node_vars = ["x{} : {}input{}".format(i,t,i) for i in range(1,num_inducers+1)] + ["x{} :".format(i,i) for i in range(num_inducers+1,num_nodes+1)]
    # The dummy node in the following is included only to workaround the issues with dsgrn_net_gen. Will be stripped before dsgrn_net_query.
    output = ["output : : E\nD : output + D : E"]
    seed_spec = " : E\n".join(inducer_vars + node_vars + output)
    return seed_spec


def strip_network_dummy(spec):
    # Remove dummy node used in net gen in order to query for dynamics
    lines = spec.split("\n")
    new_lines = []
    for line in lines:
        if line.startswith("D") or not line:
            continue
        else:
            new_lines.append(line)
    return "\n".join(new_lines)


def construct_edges(type_reg,num_nodes,num_inputs):
    regs = []
    if set(type_reg).intersection(["NOT","NOR","NAND","IMPLY","NIMPLY","activator wins", "repressor wins"]):
        regs.append("r")
    if set(type_reg).intersection(["ID","OR","AND","IMPLY","NIMPLY","activator wins", "repressor wins"]):
        regs.append("a")
    edge_str = ""
    for i in range(1,num_nodes+1):
        for r in regs:
            edge_str += "output = {}(x{})\n".format(r,i)
        for j in range(num_inputs+1,num_nodes+1):
            if i != j:
                for r in regs:
                    edge_str += "x{} = {}(x{})\n".format(j,r,i)
    return edge_str


def extra_inducer_edges(num_inducers,type_inducers):
    t = "r" if type_inducers.startswith("r") else "a"
    edge_str = ""
    for i in range(1,num_inducers+1):
        for j in range(1,num_inducers+1):
            if i != j:
                edge_str += "x{} = {}(input{})\n".format(j,t,i)
    return edge_str


def calculate_num_params(num_inducers,num_nodes):
    return 6*num_inducers + 1000*(num_nodes+1)


def calculate_range_ops(num_nodes):
    return [num_nodes,num_nodes*2]


def set_defaults(interface_params):
    defaults = {}
    if "num_inputs" not in interface_params:
        defaults["num_inputs"] = 2
    if "time_to_wait_in_seconds" not in interface_params:
        defaults["time_to_wait_in_seconds"] = 10
    if "num_cores" not in interface_params:
        defaults["num_cores"] = 1
    return defaults


def construct_file_contents(interface_params,resultsdir):
    interface_params.update(set_defaults(interface_params))
    if interface_params["num_inputs"] != 2:
        raise RuntimeError("Only 2 input circuits are implemented at this time.")
    datetimestr = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    if resultsdir.endswith("/"):
        resultsdir = resultsdir[:-1]
    resultsdir = os.path.join(resultsdir,"dsgrn_design_results{}".format(datetimestr))
    os.makedirs(resultsdir, exist_ok=True)
    seed_spec = construct_seed(interface_params["num_inputs"],interface_params["type_inputs"],interface_params["num_interior_gene_products"])
    edge_str = construct_edges(interface_params["gene_product_regulation"],interface_params["num_interior_gene_products"],interface_params["num_inputs"])
    seedfile = os.path.join(resultsdir,"seed_network.txt")
    edgesfile = os.path.join(resultsdir,"edges_list.txt")
    net_query_params = json.load(open("src/resources/params_net_query_template.json"))
    net_query_params["included_bounds"],net_query_params["excluded_bounds"] = cll.construct_FPs(interface_params["circuit"])
    net_query_params["datetime"] = datetimestr
    net_gen_params = json.load(open("src/resources/params_net_gen_template.json"))
    net_gen_params["time_to_wait"] = interface_params["time_to_wait_in_seconds"]
    net_gen_params["range_operations"] = calculate_range_ops(interface_params["num_interior_gene_products"])
    net_gen_params["maxparams"] = calculate_num_params(interface_params["num_inputs"],interface_params["num_interior_gene_products"])
    net_gen_params["resultsdir"] = resultsdir
    net_gen_params["datetime"] = datetimestr
    net_gen_params["networkfile"] = seedfile
    net_gen_params["edgefile"] = edgesfile
    return seed_spec, edge_str, net_gen_params, net_query_params,resultsdir,datetimestr,interface_params


def make_files(interface_params_json,resultsdir):
    interface_params = json.load(open(interface_params_json))
    seed, edges, net_gen, net_query,resultsdir,datetimestr,interface_params = construct_file_contents(interface_params,os.path.abspath(resultsdir))
    net_gen_file = os.path.join(resultsdir,"params_net_gen.json")
    net_query_file = os.path.join(resultsdir,"params_net_query.json")
    with open(net_gen["networkfile"],"w") as f: f.write(seed)
    with open(net_gen["edgefile"],"w") as f: f.write(edges)
    json.dump(net_gen,open(net_gen_file,"w"))
    json.dump(net_query,open(net_query_file,"w"))
    return net_gen_file, net_query_file, resultsdir,datetimestr,interface_params

