import dsgrn_design.identify_parameters as identify
import json,os,DSGRN, graphviz, coolname, ast
from copy import deepcopy
from dsgrn_utilities import graphtranslation as gt
import networkx as nx


def strip_network_inputs(spec):
    # Remove inducer self-edges for output visualization
    # do AFTER get_logic()
    lines = spec.split("\n")
    new_lines = []
    for line in lines:
        if line.startswith("in"):
            words = line.split(":")
            new_lines.append("{} : : E".format(words[0]))
        else:
            new_lines.append(line)
    return "\n".join(new_lines)


def draw_graph(network_spec,path2gvfile):
    network = DSGRN.Network(network_spec)
    graphviz.Source(network.graphviz(), filename=path2gvfile, format="png")
    return network.graphviz()


def isomorphism_checker(spec1, spec2):
    # FIXME: ideally want canonicalization here
    graph2 = gt.netspec2nxgraph(spec2)
    graph1 = gt.netspec2nxgraph(spec1)
    return nx.is_isomorphic(graph1,graph2,edge_match=lambda g,h : g == h)


def get_unique_nonzero(path2results):
    query_results = json.load(open(path2results))
    nonzero = [(r,c) for r,c in query_results.items() if c[0] > 0]
    if nonzero:
        minimal_networks = [nonzero[0][0]]
        for net,_ in nonzero[1:]:
            record = True
            for rep in minimal_networks:
                if isomorphism_checker(net,rep):
                    record = False
                    break
            if record:
                minimal_networks.append(net)
        uniq_nz = [r for r in nonzero if r[0] in minimal_networks]
    else:
        uniq_nz = None
    return uniq_nz


def get_logic(list_of_networks,included_bounds,excluded_bounds):
    # NOTE: This is computationally expensive -- only do on nonzero networks
    logic_results = identify.do_circuit_search(list_of_networks,included_bounds, excluded_bounds)
    logic_dict = {}
    for network,logics in logic_results.items():
        reorganized_logics = {}
        for p,l in logics.items():
            logstr = str(dict(sorted(l.items())))
            if logstr not in reorganized_logics:
                reorganized_logics[logstr] = [p]
            else:
                reorganized_logics[logstr].append(p)
        logic_dict[network] = reorganized_logics
    return logic_dict


def get_stripped_logic(network_specs,in_bounds,ex_bounds,regs,num_inputs,path2output):
    logic_dict = get_logic(network_specs,in_bounds,ex_bounds)
    stripped_logic_dict = {strip_network_inputs(net) : [ast.literal_eval(l) for l in logic.keys()] for net,logic in logic_dict.items()}
    outfile = os.path.join(path2output, "logic_info.json")
    json.dump(stripped_logic_dict,open(outfile,"w"))
    filtered_logics = filter_logic_by_build_constraints(logic_dict,regs,num_inputs)
    return filtered_logics


def filter_logic_by_build_constraints(logic_dict,allowed_node_logic,num_inputs):
    filtered_logics = {}
    anl = set(allowed_node_logic)
    for net,logics in logic_dict.items():
        logs = []
        param_matches = []
        for lg,params in logics.items():
            lg_dict = ast.literal_eval(lg)
            temp = deepcopy(lg_dict)
            # don't filter over inputs and input targets. Those logics are not constrained.
            for i in range(1,num_inputs+1):
                temp.pop("input{}".format(i))
                temp.pop("x{}".format(i))
            _,l = zip(*temp.items())
            if set(l).issubset(anl):
                logs.append(lg_dict)
                param_matches.extend(params)
        if logs:
            filtered_logics[net] = (logs,param_matches)
    return filtered_logics


def present_output(path2output,path2networks,path2queryresults,path2queryparameters,path2interfaceparameters):
    results = get_unique_nonzero(path2queryresults)
    if results:
        interface_params = json.load(open(path2interfaceparameters))
        query_params = json.load(open(path2queryparameters))
        in_bounds, ex_bounds = query_params["included_bounds"], query_params["excluded_bounds"]
        regs, num_inputs = interface_params["gene_product_regulation"][:], interface_params["num_inputs"]
        truth_table_file = os.path.join(path2output,"truth_table.json")
        json.dump(interface_params["circuit"],open(truth_table_file,"w"))
        networks = [r[0] for r in results]
        logic_dict = get_stripped_logic(networks,in_bounds,ex_bounds,regs,num_inputs,path2output)
        filtered_networks = []
        parameter_matches = []
        for net,logics in logic_dict.items():
            filtered_networks.append(net)
            parameter_matches.append(logics[1])
        #FIXME: Ideally, we want to remove the second threshold order for the inputs, since the self-edge is a workaround and doesn't really have meaning.
        # However, I believe that all it does is multiply everything by 4, and therefore when we take a ratio for scoring, there is no cumulative effect.
        # parameter_matches are already filtered to be design parameters
        neighbor_score_dict = identify.do_circuit_search(filtered_networks, in_bounds, ex_bounds,parameter_matches)
        neighbor_save = os.path.join(os.path.split(path2queryresults)[0],"query_results_neighbors.json")
        json.dump(neighbor_score_dict,open(neighbor_save,"w"))
        results = dict(results)
        filtered_results = {}
        for net,pm in zip(filtered_networks,parameter_matches):
            filtered_results[strip_network_inputs(net)] = [results[net],neighbor_score_dict[net],len(pm)]
        logic_dict = {strip_network_inputs(net): lgc[0] for net, lgc in logic_dict.items()}
        summary_file = get_summary(filtered_results,path2output, path2networks,logic_dict,interface_params["circuit"])
    else:
        summary_file = None
    return summary_file


def get_summary(filtered_results,path2output, path2networks,logic_dict,circuit):
    print("Creating output files...")
    summary = {}
    for net,counts in filtered_results.items():
        name = coolname.generate_slug(2).replace("-", "_")
        fname = os.path.join(path2networks,"network_{}.txt".format(name))
        open(fname,"w").write(net)
        pic_file = fname[:-4]+".gv"
        dot_src = draw_graph(net,pic_file)
        logic_index = []
        for k, lg in enumerate(logic_dict[net]):
            logic_index.append((k+1, lg))
        summary[net] = {"circuit" : circuit,"essential_parameter_matches": counts[0][0], "essential_parameter_graph_size": counts[0][1], "neighbor_design_parameter_matches": counts[1][0], "neighbor_design_parameter_graph_size": counts[1][1], "design_parameter_matches" : counts[2], "network_file": fname, "graphviz_dot": dot_src,"gene_regulation": logic_index}
    summary_file = os.path.join(path2output,"summary.json")
    json.dump(summary,open(summary_file,"w"))
    return summary_file






