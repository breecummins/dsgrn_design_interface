# docker pull continuumio/anaconda3
FROM continuumio/anaconda3

# environment
ENV PATH /opt/conda/bin/:/bin/:$PATH

# apt packages
# -y answers "yes" to everything for noninteractive installation
RUN apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y make vim gcc g++ cmake openmpi-bin libopenmpi-dev

# copy files
WORKDIR /dsgrn_design_interface
COPY ./*.* /dsgrn_design_interface/
COPY ./install_resources/*.* ./install_resources/
COPY ./src/ ./src/
COPY ./scripts/ ./scripts/
COPY ./tests/*.* ./tests/

# conda env
RUN conda env create -f /dsgrn_design_interface/install_resources/conda_req.yml
RUN ["/bin/bash", "-c", "source activate dsgrndesign" ]
ENV PATH /opt/conda/envs/dsgrndesign/bin:$PATH
ENV CONDA_DEFAULT_ENV dsgrndesign

# install dsgrn design, which installs many sub-packages
RUN . /dsgrn_design_interface/install.sh

# non-root user (mpiexec does not like running as root)
RUN useradd -m --no-log-init dsgrn_user
RUN chown -R dsgrn_user: /dsgrn_design_interface
USER dsgrn_user

# run tests
RUN cd /dsgrn_design_interface/tests \
&& . ./run_tests.sh

