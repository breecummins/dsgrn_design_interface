from dsgrn_design.sbol_parser import make_doc
import os,sys,json


def make_sbol_document(stripped_network_file,logic_dict,truth_table_file,sbol_file):
    return make_doc(stripped_network_file,logic_dict, truth_table_file, output_fname = sbol_file)


def make_sbol_files(results_dir):
    datetime = results_dir.split('results')[-1]
    path2output = os.path.join(results_dir,"design_results{}".format(datetime))
    summary = json.load(open(os.path.join(path2output, "summary.json")))
    truth_table_file = os.path.join(path2output,"truth_table.json")
    for net in summary:
        fname = summary[net]["network_file"]
        # handle situation where the file has the Docker path
        # This should work because the results directory must be in the current path to mount in the Docker image
        if fname.startswith("/dsgrn_design_interface/"):
            fname = fname.replace("/dsgrn_design_interface/","")
        logic_index = summary[net]["gene_regulation"]
        sbol_docs = []
        sbol_urls = []
        for k,lg in logic_index:
            sbol_file = fname[:-4] + "_node_logic_{:02}.xml".format(k)
            url = make_sbol_document(fname,lg,truth_table_file,sbol_file)
            sbol_docs.append(sbol_file)
            if url is not None:
                sbol_urls.append(url)
        summary[net]["sbol_documents"] = sbol_docs
        if sbol_urls:
            summary[net]["sbol_urls"] = sbol_urls
    summary_file = os.path.join(path2output,"summary.json")
    json.dump(summary,open(summary_file,"w"))


if __name__ == "__main__":
    helpstr = """
    This function has one required input argument:
    (1) A path to to the dsgrn_design_results<datetime> folder that is to be reanalyzed.
    """
    make_sbol_files(sys.argv[1])

