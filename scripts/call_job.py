from dsgrn_design import interface
import sys,os,json,subprocess,ast
from dsgrn_net_gen.makejobs import Job
from dsgrn_design.generate_output import present_output
from dsgrn_design.interface import strip_network_dummy


def trigger_net_gen(net_gen_file):
    job = Job(net_gen_file)
    job.run()
    ng = json.load(open(net_gen_file))
    network_file = os.path.join(ng["resultsdir"], "dsgrn_net_gen_results{}/networks{}/networks.txt".format(ng["datetime"],ng["datetime"]))
    return network_file


def trigger_net_query(max_threads,net_query_file,resultsdir,network_file):
    query = os.path.abspath("./src/resources/CountFPMatch.py")
    os.system("mpiexec -n {} python {} {} {} {}".format(max_threads, query, network_file, net_query_file, resultsdir))


def main(interface_params_file,resultsdir,notebook="./src/resources/Output_Visualization.ipynb"):
    net_gen_file, net_query_file,resultsdir,datetime,interface_params = interface.make_files(interface_params_file,resultsdir)
    print("\nGenerating networks....\n")
    network_file = trigger_net_gen(net_gen_file)
    networks = ast.literal_eval(open(network_file).read())
    # strip out dummy node for network queries
    # dummy node is a work-around for net gen
    networks = [strip_network_dummy(net) for net in networks]
    json.dump(networks,open(network_file,"w"))
    print("Generating networks complete.\n")
    print("Querying networks...\n")
    trigger_net_query(interface_params["num_cores"],net_query_file,resultsdir,network_file)
    print("Querying complete.\n")
    subprocess.call("rm {}".format(os.path.join(resultsdir,"*.txt")),shell=True)
    subprocess.call("rm {}".format(os.path.join(resultsdir,"*.json")),shell=True)
    print("Analyzing...\n")
    path2results = os.path.join(resultsdir,"dsgrn_net_query_results{}/queries{}/query_results.json".format(datetime,datetime))
    path2queryparams = os.path.join(resultsdir,"dsgrn_net_query_results{}/inputs{}/{}".format(datetime,datetime,os.path.split(net_query_file)[-1]))
    path2input = os.path.join(resultsdir,"inputs{}".format(datetime))
    os.makedirs(path2input, exist_ok=True)
    interface_params_file = os.path.join(path2input,"interface_params.json")
    json.dump(interface_params,open(interface_params_file,"w"))
    path2output = os.path.join(resultsdir,"design_results{}".format(datetime))
    os.makedirs(path2output, exist_ok=True)
    path2networks = os.path.join(path2output,"networks")
    os.makedirs(path2networks, exist_ok=True)
    summary_file = present_output(path2output,path2networks,path2results,path2queryparams,interface_params_file)
    print("Analysis complete.\n")
    if summary_file:
        os.system("cp {} {}".format(os.path.abspath(notebook),path2output))
        new_notebook_path = os.path.join(path2output,notebook.split("/")[-1])
        print("Generating Jupyter notebook...\n")
        os.system("jupyter nbconvert --execute {} --to notebook".format(new_notebook_path))
        os.system("rm {}".format(new_notebook_path))
        jupyter_path = os.path.abspath(new_notebook_path).split(".")[0] + ".nbconvert.ipynb"
        print("Output generated in {}.\nJupyter notebook: {}.\n".format(resultsdir, jupyter_path))
        return path2results, summary_file, jupyter_path
    else:
        print("No results found. Retry with altered parameters.")


if __name__ == "__main__":
    helpstr = """
    This function takes two input arguments:
    (1) A path to an interface parameter file.
    (2) A path to a results folder in which to write output.
    """
    main(sys.argv[1],sys.argv[2])
