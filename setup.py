from distutils.core import setup

setup(
    name='dsgrn_design_interface',
    package_dir={'':'src'},
    packages = ["dsgrn_design"],
    install_requires=[],
    author="Bree Cummins",
    url='https://gitlab.sd2e.org/bcummins/dsgrn_design_interface'
    )