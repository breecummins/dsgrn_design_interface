# DSGRN Design Interface

This Python module is used to produce designs for feed-forward circuits that satisfy user-supplied truth tables. Currently only certain truth tables for two inputs are specified: AND, OR, NAND, NOR, XOR, XNOR, IMPLY, and NIMPLY. The output designs are subject to user-supplied constraints.

## Getting Started

The codebase consists of Python 3 modules (and C bindings) maintained in several stand-alone repositories. 

There are two options for installation: installation via a conda environment and installation via a Dockerfile. If you plan to script around the available packages or want to run code through a Jupyter notebook, try the local installation. Otherwise, the docker version is much easier to install. 

### Installation via Docker

First install Docker if you do not have it: https://docs.docker.com/get-docker/. Then clone the repository and build the Docker image from the provided Dockerfile.

```bash
$ git clone https://gitlab.sd2e.org/bcummins/dsgrn_design_interface.git
$ cd dsgrn_design_interface
$ docker pull continuumio/anaconda3
$ docker build --rm -f install_resources/Dockerfile -t dsgrn_design_docker .
```

If `mpiexec` will not run inside of the container, try  `Dockerfile2` instead. You can check if `mpiexec` runs by doing the following:

```bash
$ docker run --rm -it dsgrn_design_docker
$ source activate dsgrndesign
$ cd packages/dsgrn_net_query/tests/
$ pytest
```

Generally speaking, if those tests fail it is because `C` libraries needed for `mpiexec` cannot be found, and this might be remedied by `Dockerfile2`.

In order to view the output Jupyter notebooks, you'll need Python 3, Jupyter, and graphviz. 

* Interpreter  

    For both MacOS and Ubuntu: If you do not have Anaconda3 or miniconda3 installed, do so now: https://docs.anaconda.com/anaconda/install/.

* Package Manager   

    If you are on MacOS and don't have HomeBrew installed:
    ```bash
    $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    ```

* graphviz  
    
    If you do not have `graphviz` and `dot` installed:
    
    For MacOS:
    ```bash
    $ brew install graphviz
    ```
    For Ubuntu:
    ```bash
    $ sudo apt install graphviz
    ```
  
 * Python packages
 
    For both Mac and Ubuntu, install the following packages:
    ```bash
    $ conda install -c conda-forge jupyterlab
   $ conda install -c anaconda python-graphviz
    ```  
  
You may delete the folder `dsgrn_design_interface` when you are done, perhaps saving this `README`. If you need to update or rebuild the Docker image, you will have to clone the repository again.



### Installation with conda environment

#### 1) Install Prerequisites

Installation has been developed and tested for MacOS Mojave and Ubuntu 18.

* Interpreter  

    For both MacOS and Ubuntu: If you do not have Anaconda3 or miniconda3 installed, do so now: https://docs.anaconda.com/anaconda/install/.

  
* C Compilers
    
    For MacOS: install the Xcode CommandLineTools:
    
  ```bash
  $ xcode-select install
  ```
  or
  ```bash
  $ xcode-select --install
  ```
  
    For Ubuntu:
    ```bash
  $ sudo apt install gcc g++
    ```
* Package Manager   

    If you are on MacOS and don't have HomeBrew installed:
  ```bash
  $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    ```

* MPI  
    
    If you do not have  `MPI` installed:
    
    For MacOS:
    ```bash
    $ brew install openmpi
    ```
    For Ubuntu:
    ```bash
    $ sudo apt install openmpi-bin
    ```

* cmake  
    
    If you do not have `cmake` installed:
    
    For MacOS:
    
    ```bash
    $ brew install cmake
  ```
    
    For Ubuntu:
    ```bash
    $ sudo apt -y install cmake
    ```
  
* graphviz  
    
    If you do not have `graphviz` and `dot` installed:
    
    For MacOS:
    ```bash
    $ brew install graphviz
    ```
    For Ubuntu:
    ```bash
    $ sudo apt install graphviz
    ```

#### 2) Install Interface

To ensure compatibility across platforms and with required packages, a conda environment should first be created using the supplied `conda_req.yml` file and activated before running the custom Python installation script. 
```bash
$ git clone https://gitlab.sd2e.org/bcummins/dsgrn_design_interface.git
$ cd dsgrn_design_interface
$ conda env create -f install_resources/conda_req.yml
$ conda activate dsgrndesign
$ . install.sh
```
If `conda activate dsgrndesign` does not work, try `source activate dsgrndesign`. If `. install.sh` fails with `no such file`, try `source install.sh`.

These steps will install the required Python packages to the environment `dsgrndesign` so as to not interfere with existing packages you may have installed. The installation script clones several public Gitlab repositories. Local copies of these repositories will be added to the folder `packages`. Please ensure that the conda environment `dsgrndesign` is active prior to installing or running any part of the module.


#### 3) Troubleshooting

**For Ubuntu:**

It is common to have issues with C tooling during this installation. An alternative is to try using C tools that are installed through Anaconda instead of through a package manager. This has been used as a solution for installing on Ubuntu and on remote Linux clusters (with mixed success). The following commands install C and C++ compilers within your `dsgrndesign` conda environment. 

```bash
$ conda activate dsgrndesign
$ conda install -y -c conda-forge gcc_linux-64 gxx_linux-64
```

In order for these tools to be visible for installation, you'll need to symlink them into the common names `gcc` and `g++`. 
```bash
$ ln -s <your_path>/anaconda3/envs/dsgrndesign/bin/x86_64-conda_cos6-linux-gnu-gcc <your_path>/anaconda3/envs/dsgrndesign/bin/gcc
$ ln -s <your_path>/anaconda3/envs/dsgrndesign/bin/x86_64-conda_cos6-linux-gnu-g++ <your_path>/anaconda3/envs/dsgrndesign/bin/g++
```

**Note:** These conda compilers will be used whenever the `dsgrndesign` conda environment is active. To use system-level C tools, deactivate the `dsgrndesign` conda environment, `conda deactivate`.

**Note:** The executable names `x86_64-conda_cos6-linux-gnu-g**` may change and have to be altered in the above commands.

After the compilers are linked, install the rest of the C tools.
```bash
$ conda install -y -c conda-forge cmake openmpi mpi4py
```

Now try installing:
```bash
$ . install.sh
```

**For Mac OS X:**

A similar solution for Mac OS X would be to install the packages `clang_osx-64` and `clangxx_osx-64` instead of `gcc_linux-64` and `gxx_linux-64` and perform the symlink operation with the appropriate compiler names. This has not been tested.

#### 4) Run Tests
Tests of the git packages `min_interval_posets`, `dsgrn_utilities`, `dsgrn_net_gen`, `dsgrn_net_query`, and `dsgrn_design_interface` can be performed by running 
```bash
$ cd tests
$ . run_tests.sh
```

## Circuit design commands

**Conda environment via commandline:**
To perform a circuit design using the conda environment from the commandline, run the following. (Conda environment activation and changing directory commands are provided as reminders.)
```bash
$ conda activate dsgrndesign
$ cd dsgrn_design_interface
$ python scripts/call_job.py <interface_params.json> <results_dir> 
```



**Conda environment via Jupyter notebook:**
See `usage/Tutorial.ipynb`. The chosen environment for the notebook must be `dsgrndesign`.

**Docker option:**
First note that your `<results_dir>` must already be created in `dsgrn_design_interface`.
```bash
$ cd dsgrn_design_interface
$ docker run -v "$(pwd)/<results_dir>:/dsgrn_design_interface/<results_dir>" dsgrn_design_docker python scripts/call_job.py <interface_params.json> <results_dir> 
```


### Input

* **Argument 1:** interface parameter `json` file

See the `json` schema in the `usage` folder.

Example:

```json
  {
  "circuit": "AND",
  "num_inputs" : 2,
  "type_inputs" : "activators",
  "num_interior_gene_products" : 4,
  "gene_product_regulation": ["NOT","NOR"],
  "time_to_wait_in_seconds": 30,
  "num_cores" : 4
  }
```

<!--   "multiple_inputs_at_a_node" : false,
 -->

`circuit` = `AND`, `OR`, `NAND`, `NOR`, `XOR`, `XNOR`, `IMPLY`, or `NIMPLY` logic in quotes. Expected to be extended to arbitrary truth tables in the future.

`num_inputs` = `integer` (optional). Default = `2`. The number of inputs to the circuit. Currently only two input circuits are implemented, but this is expected to be extended in the future. 

`type_inputs` = `activators` or `repressors` in quotes. Mixed input types are not currently implemented.

<!-- `multiple_inputs_at_a_node` : `true` or `false`. Whether or not a circuit node is allowed to accept more than one of the inputs.  -->

`num_interior_gene_products`= `integer`. This is the number of nodes in the network, excluding input molecules and the output (fluorescence) marker. As the number gets higher, the algorithm does not scale well. As a heuristic, don't exceed about 12.

`gene_product_regulation`: `list of node logics`. These are constraints on how individual combinations of inputs are allowed to act. If single repressors are a legitimate build part, then the logic is `NOT`. For two repressors where each of them can independently suppress transcription, the logic is `NOR`. If both repressors are required to suppress transcription, then the logic is `NAND`. A single activator is the logic `ID`. If two activators are allowed to be present and both are necessary for downstream transcription, then `AND` is the appropriate logic. If only one is necessary, then the logic is `OR`. If both an activator and a repressor are present, then one or the other may dominate activity. The node logics are `IMPLY` or `activator wins` and `NIMPLY` or `repressor wins`. The logical functions `XOR` and `XNOR` are disallowed as they cannot be computed in DSGRN at this time.

`time_to_wait_in_seconds` = `number` (optional). Default = `10`. It takes a certain amount of time to generate the network sample with the provided circuit constraints. The number of networks sought is 2000. If the network generation process times out while the number of networks found is still increasing, then increase the number of seconds to wait. If the number of networks is not increasing, then it may be that all or nearly all networks have been located already. Note that 10 seconds is probably too short for `num_interior_gene_products >= 5`. 

`num_cores` = `integer` (optional). Default = `1`. Maximum number of processes to run during the DSGRN query step. Do not exceed available cores or `mpiexec` may throw an error.

* **Argument 2:** results directory

This is a directory that exists in your path where the output from the circuit design will be stored. For calls using the `conda` environment, the results directory can be anywhere in your file system as long as you specify the full path to the directory. For calls using `Docker`, the results directory must exist in _your current directory_. This has to do with how Docker handles permissions.

### Output

The output is saved in a subfolder within the specified `<results_dir>` folder. The subfolder is named `dsgrn_design_results<datetime>`, ending in a unique date-time stamp to avoid overwriting previous results in the same folder. There are four subfolders inside `dsgrn_design_results<datetime>`:

* `inputs<datetime>` 

    *   This folder contains a copy of the interface parameter `json` file.
    
* `dsgrn_net_gen_results<datetime>` and `dsgrn_net_query_results<datetime>`
    *   These folders contain intermediate results produced by subpackages.

* `design_results<datetime>` 
    *   This folder contains the main results of the program. The subfolder `networks` contains a DSGRN `.txt` file for each network that both satisfies the supplied build constraints and has a nonzero **design score**. (More on scoring below.)
    
         The folder also includes the following files:
         * The Jupyter notebook `Output_Visualization.nbconvert.ipynb`. This contains pictures, network scores, and parts specifications (gene product regulation and constitutive expression) for networks with a nonzero design score. It may be necessary to rerun the notebook to see the graphics. The networks are rank-ordered according to a **network robustness score**.
         * The file `logic_info.json`. This contains a `json` dictionary where the keys are regulatory networks in DSGRN format. Every network in the dictionary has a nonzero score for the behavior of the logic circuit. However, not all these networks will satisfy the build constraints. The dictionary values are the gene product regulation at each node, which allow the networks to be filtered for satisfying build constraints.
         * The file `summary.json`. This is a `json` dictionary keyed by DSGRN network specification strings that satisfy the build constraints. Each network is associated to a dictionary of information.
            * The circuit logic for the design.
            * The file name containing the DSGRN network format.
            * The graphviz string for the regulatory network (for viewing).
            * The gene regulation at each node.
            * The size of the DSGRN parameter graph for the essential regulatory network, the number of DSGRN design parameters satisfying the logic circuit design, the number of neighbors of these design parameters, and the number of these neighbors that also show the desired circuit behavior. Most of these numbers are used in the network score described below.
            
### Network scoring
            
##### DSGRN design parameters

A DSGRN parameter is a region in high-dimensional, real-valued, positive parameter space. The long-term network dynamics (i.e. circuit function) are invariant across each region. There are a finite number of regions, which allows for normalized circuit performance across parameter space.

Some of the DSGRN parameters satisfy the build constraints given by `gene_product_regulation` and also exhibit the desired circuit function. These are called **design parameters**.

Design parameters are a subset of DSGRN **essential parameters**, which encompass all possible combinations of the logics consistent with the network topology that are listed in the `gene_product_regulation` key of the `json` input file. For example, if node has two incoming activators, then `OR` and `AND` are both essential parameters. Slight variants of these logics are also deemed essential when the node has more than one downstream target.

##### DSGRN parameter neighbors

DSGRN parameters can be organized into graph where an edge between parameter nodes is present whenever the associated DSGRN parameter regions are adjacent in high-dimensional space. Therefore, the idea of **neighboring parameters** to the design parameter is well-defined. These neighbors roughly capture what happens when there is a single failure of a circuit edge. The number of neighbors that expresses the correct circuit function allows an estimate of the robustness of the combined circuit and design parameter.   

##### Score computations

The four scores introduced below all range from 0 to 1. These scores should NOT be interpreted as probabilities. They are guides for ranking networks only.

$$ \text{Network robustness score} = \frac{D_m + N_m}{E + N} $$

where $D_m$ is the number of design parameters that are predicted to exhibit the correct circuit behavior, called **matching** parameters, $N_m$ is the number of matching neighbors to any one or more of the design parameters, $E$ is the number of essential parameters, and $N$ is the total of neighbors to the set of design parameters. The network robustness score determines the ranking given in `Output_Visualization.nbconvert.ipynb`. 

Other scores are provided for the user as well. 

$$ \text{Essential score} = \frac{E_m}{E} $$

where $E_m$ is the number of matching essential parameters, which recall is a superset of the design parameters and may not be consistent with the input build constraints.

$$ \text{Design score} = \frac{D_m}{E} $$

This score is the fraction of matching design parameters in the set of all essential parameters. The design score must be nonzero or else the network robustness score is not computed.

$$ \text{Design perturbation score} = \frac{D_m + N_m}{D_m + N} $$

This score ranks the prevalence of single edge failures given that the circuit is designed perfectly, but fails under some experimental condition.    
    
## Creation of SBOL documents
The Synthetic Biology Open Language (SBOL) is an international standard, https://sbolstandard.org/. To create `SBOL2` compatible `.xml` documents for DSGRN networks, do the following.

**For the conda environment:**
```bash
$ conda activate dsgrndesign
$ cd dsgrn_design_interface
$ python scripts/make_sbol_docs.py <design_results_dir>
```
Conda environment activation and changing directory commands are provided as reminders. 

**For the Docker image:**
```bash
$ docker run -v "$(pwd)/<results_dir>:/dsgrn_design_interface/<results_dir>" dsgrn_design_docker python scripts/make_sbol_docs.py <design_results_dir>
```

The `<design_results_dir>` is the top level directory within `results_dir` that contains the date-time stamp. For example: `./results/dsgrn_design_results20201006084919`.  The output is a collection of SBOL documents in the `.xml` format in the `<design_results_dir>/networks/` folder, one for each existing network file.

The SBOL document file names will be added to the `summary.json` file.
        
## Tips

1. If no networks are produced, there are several things to try.
    
   * It may be that the build constraints are too restrictive, making it impossible to generate networks with the desired behavior. Some potential problems:
       * All allowed parts are activators. Feed-forward logic circuits can only be created if there are some repressors.
       * More generally, there could be insufficient variety in the allowed behavior at the nodes. For example, if `gene_product_regulation` = `["NAND"]`, then not all circuits can be constructed.
       * There could be too few interior nodes. Try increasing `num_interior_gene_products`.
   * There might be insufficient time to generate networks. Try increasing `time_to_wait_in_seconds`.

2. If the number of networks is still increasing rapidly at the time that 2000 networks are located and the network generation process ends, consider doing another run to get another sample size of 2000. This code is stochastic and will likely pick up more networks than the original 2000.
