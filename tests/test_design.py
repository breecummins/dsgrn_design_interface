import os,json,sys
sys.path.append(os.path.abspath("../scripts"))
from call_job import main
from make_sbol_docs import make_sbol_files
from dsgrn_design.generate_output import isomorphism_checker



def test1():
    net1 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x2) : E\nx4 : (~x1) : E"
    net11 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x1) : E\nx4 : (~x2) : E"
    net2 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x2)(~x4) : E\nx4 : (~x1) : E"
    net21 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x1)(~x4) : E\nx4 : (~x2) : E"
    net22 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x2) : E\nx4 : (~x1)(~x3) : E"
    net23 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x1) : E\nx4 : (~x2)(~x3) : E"
    assert(isomorphism_checker(net11,net1))
    assert(not isomorphism_checker(net1,net2))
    assert(isomorphism_checker(net21,net2))
    assert(isomorphism_checker(net22,net2))
    assert(isomorphism_checker(net23,net2))


def test2(erase=True):
    os.chdir("..")
    path2results, summaryfile,jupyter_path = main("tests/interface_params.json","tests")
    path2output = "/".join(jupyter_path.split("/")[:-1])
    path2networks = os.path.join(path2output,"networks")
    names1 = [x for x in os.listdir(path2networks) if x.endswith(".txt")]
    assert(len([x for x in os.listdir(path2networks) if x.endswith(".xml")]) == 0)
    results = json.load(open(path2results))
    assert(len(results) == 55)
    summary1 = json.load(open(summaryfile))
    assert(len(summary1)==2)
    net1 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x2) : E\nx4 : (~x1) : E"
    net2 = "input1  : : E\ninput2  : : E\noutput : (~x3)(~x4) : E\nx1 : (input1) : E\nx2 : (input2) : E\nx3 : (~x2)(~x4) : E\nx4 : (~x1) : E"
    for net in summary1:
        assert(isomorphism_checker(net,net1) or isomorphism_checker(net,net2))
    results_dir = "/".join(jupyter_path.split("/")[:-2])
    logic_file = os.path.join("/".join(summaryfile.split("/")[:-1]),"logic_info.json")
    logics = json.load(open(logic_file))
    assert(len(logics)==5)
    make_sbol_files(results_dir)
    assert(len([x for x in os.listdir(os.path.join(path2output,"networks")) if x.endswith(".xml")]) == 2)
    os.chdir("tests/")
    if erase:
        os.system("rm -r dsgrn_design_results*")


if __name__ == "__main__":
    # these tests just check that the code runs, not necessarily that it is correct
    # can take a long time to run, don't do during pytest
    def test3():
        os.chdir("..")
        main("tests/interface_params2.json", "tests")
        os.chdir("tests/")

    def test4():
        os.chdir("..")
        main("tests/interface_params_defaults.json", "tests")
        os.chdir("tests/")

    def test5():
        os.chdir("..")
        main("tests/interface_params_parallel.json", "tests")
        os.chdir("tests/")

    test2(erase=False)
    # test3()










